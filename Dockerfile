FROM centos:8

RUN dnf install -y rpm-build redhat-rpm-config openssh-clients openssh-clients
RUN mkdir -p $HOME/rpmbuild/RPMS/X86_64 \
             $HOME/rpmbuild/SOURCES \
             $HOME/rpmbuild/SPECS \
             $HOME/rpmbuild/SRPMS

